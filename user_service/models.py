from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token


class Account(BaseModel):
    first: str
    last: str
    age: int
    email: str
    username: str


class AccountOut(Account):
    id: int
    modified: str
    hashed_password: str


class AccountIn(Account):
    password: str


class AccountToken(Token):
    account: AccountOut
